// @flow
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { start } from '../../actions/session';
import VisitorForm from '../../components/VisitorForm';
import Navbar from '../../components/Navbar';

type Props = {
  start: () => void,
}

class Visitor extends Component {
  static contextTypes = {
    router: PropTypes.object,
  }

  props: Props

  handleLogin = (data) => this.props.start(data, this.context.router);

  render() {
    return (
      <div style={{ flex: '1' }}>
        <Navbar />
        <VisitorForm onSubmit={this.handleLogin} />
      </div>
    );
  }
}

export default connect(null, { start })(Visitor);
