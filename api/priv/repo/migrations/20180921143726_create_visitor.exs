defmodule LiveChat.Repo.Migrations.CreateVisitor do
  use Ecto.Migration

  def change do
    create table(:visitors) do
      add :email, :string

      timestamps()
    end

  end
end
