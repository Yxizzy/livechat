defmodule LiveChat.SessionView do
  use LiveChat.Web, :view

  def render("show.json", %{user: user, jwt: jwt}) do
    %{
      data: render_one(user, LiveChat.UserView, "user.json"),
      meta: %{token: jwt}
    }
  end

  def render("start.json", %{visitor: visitor, jwt: jwt}) do
    %{
      data: render_one(visitor, LiveChat.VisitorView, "visitor.json"),
      meta: %{token: jwt}
    }
  end


  def render("error.json", _) do
    %{error: "Invalid email or password"}
  end

  def render("delete.json", _) do
    %{ok: true}
  end

  def render("forbidden.json", %{error: error}) do
    %{error: error}
  end
end
