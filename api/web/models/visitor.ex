defmodule LiveChat.Visitor do
  use LiveChat.Web, :model

  schema "visitors" do
    field :email, :string
    many_to_many :rooms, LiveChat.Room, join_through: "user_rooms"
    has_many :messages, LiveChat.Message

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:email])
    |> validate_required([:email])
    |> unique_constraint(:email)
  end
end
