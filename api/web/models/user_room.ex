defmodule LiveChat.UserRoom do
  use LiveChat.Web, :model

  schema "user_rooms" do
    belongs_to :user, LiveChat.User
    belongs_to :room, LiveChat.Room

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:user_id, :room_id])
    |> validate_required([:user_id, :room_id])
    |> unique_constraint(:user_id_room_id)
  end
end
