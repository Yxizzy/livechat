defmodule LiveChat.MessageController do
  use LiveChat.Web, :controller

  plug Guardian.Plug.EnsureAuthenticated, handler: LiveChat.SessionController

  def index(conn, params) do
    last_seen_id = params["last_seen_id"] || 0
    room = Repo.get!(LiveChat.Room, params["room_id"])

    page =
      LiveChat.Message
      |> where([m], m.room_id == ^room.id)
      |> where([m], m.id < ^last_seen_id)
      |> order_by([desc: :inserted_at, desc: :id])
      |> preload(:user)
      |> LiveChat.Repo.paginate()

    render(conn, "index.json", %{messages: page.entries, pagination: LiveChat.PaginationHelpers.pagination(page)})
  end
end
