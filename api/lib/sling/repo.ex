defmodule LiveChat.Repo do
  use Ecto.Repo, otp_app: :liveChat
  use Scrivener, page_size: 25
end
