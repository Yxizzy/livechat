defmodule LiveChat.GuardianSerializer do
  @behaviour Guardian.Serializer

  alias LiveChat.Repo
  alias LiveChat.User
  alias LiveChat.Visitor

  def for_token(user = %User{}), do: {:ok, "User:#{user.id}"}
  def for_token(_), do: {:error, "Unknown resource type"}

  def for_token(visitor = %Visitor{}), do: {:ok, "Visitor:#{visitor.id}"}
  def for_token(_), do: {:error, "Unknown resource type"}

  def from_token("User:" <> id), do: {:ok, Repo.get(User, String.to_integer(id))}
  def from_token(_), do: {:error, "Unknown resource type"}
end
